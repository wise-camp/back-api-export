const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);

const Action = {
    getAllActions (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM ACTION")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    getAction (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM ACTION WHERE id_action = ?",[req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    insertAction (req, res) {
        const rythme_formation = req.body.rythme_formation;
        const code_public_vise = req.body.code_public_vise;
        const info_public_vise = req.body.info_public_vise;
        const niveau_entree_obligatoire = req.body.niveau_entree_obligatoire;
        const modalites_alternance = req.body.modalites_alternance;
        const modalites_enseignement = req.body.modalites_enseignement;
        const conditions_specifiques = req.body.conditions_specifiques;
        const prise_en_charge_frais_possible = req.body.prise_en_charge_frais_possible;
        const lieu_de_formation = req.body.lieu_de_formation;
        const modalites_entrees_sorties = req.body.modalites_entrees_sorties;
        const url_action = req.body.url_action;
        const session = req.body.session;
        const adresse_information = req.body.adresse_information;
        const date_information = req.body.date_information;
        const restauration = req.body.restauration;
        const hebergement = req.body.hebergement;
        const transport = req.body.transport;
        const acces_handicapes = req.body.acces_handicapes;
        const langue_formation = req.body.langue_formation;
        const modalites_recrutement = req.body.modalites_recrutement;
        const modalites_pedagogiques = req.body.modalites_pedagogiques;
        const code_modalite_pedagogique = req.body.code_modalite_pedagogique;
        const frais_restants = req.body.frais_restants;
        const code_perimetre_recrutement = req.body.code_perimetre_recrutement;
        const infos_perimetre_recrutement = req.body.infos_perimetre_recrutement;
        const prix_horaire_ttc = req.body.prix_horaire_ttc;
        const prix_total_TTC = req.body.prix_total_TTC;
        const duree_indicative = req.body.duree_indicative;
        const nombre_heures_centre = req.body.nombre_heures_centre;
        const nombre_heures_entreprise = req.body.nombre_heures_entreprise;
        const nombre_heures_total = req.body.nombre_heures_total;
        const detail_conditions_prise_en_charge = req.body.detail_conditions_prise_en_charge;
        const conventionnement = req.body.conventionnement;
        const duree_conventionnee = req.body.duree_conventionnee;
        const organisme_formateur = req.body.organisme_formateur;
        const organisme_financeur = req.body.organisme_financeur;
        const financement_formation = req.body.financement_formation;
        const nb_places = req.body.nb_places;
        const deroulement = req.body.deroulement;
        const moyens_pedagogiques = req.body.moyens_pedagogiques;
        const responsable_enseignement = req.body.responsable_enseignement;
        const nombre_heures_cm = req.body.nombre_heures_cm;
        const nombre_heures_td = req.body.nombre_heures_td;
        const nombre_heures_tp_tuteure = req.body.nombre_heures_tp_tuteure;
        const nombre_heures_tp_non_tuteure = req.body.nombre_heures_tp_non_tuteure;
        const nombre_heures_personnel = req.body.nombre_heures_personnel;
        const nombre_heures_hebdomadaire = req.body.nombre_heures_hebdomadaire;
        const id_formation = req.body.id_formation;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO ACTION (rythme_formation, code_public_vise, info_public_vise, modalites_alternance, modalites_enseignement, niveau_entree_obligatoire, conditions_specifiques, prise_en_charge_frais_possible, lieu_de_formation, modalites_entrees_sorties, url_action, session, adresse_information, date_information, restauration, hebergement, transport, acces_handicapes, langue_formation, modalites_recrutement, modalites_pedagogiques, code_modalite_pedagogique, frais_restants, code_perimetre_recrutement, infos_perimetre_recrutement, prix_horaire_ttc, prix_total_TTC, duree_indicative, nombre_heures_centre, nombre_heures_entreprise, nombre_heures_total, detail_conditions_prise_en_charge, conventionnement, duree_conventionnee, organisme_formateur, organisme_financeur, financement_formation, nb_places, deroulement, moyens_pedagogiques, responsable_enseignement, nombre_heures_cm,  nombre_heures_td, nombre_heures_tp_tuteure, nombre_heures_tp_non_tuteure, nombre_heures_personnel, nombre_heures_hebdomadaire, id_formation) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    [rythme_formation, code_public_vise, info_public_vise, modalites_alternance, modalites_enseignement, niveau_entree_obligatoire, conditions_specifiques, prise_en_charge_frais_possible, lieu_de_formation, modalites_entrees_sorties, url_action, session, adresse_information, date_information, restauration, hebergement, transport, acces_handicapes, langue_formation, modalites_recrutement, modalites_pedagogiques, code_modalite_pedagogique, frais_restants, code_perimetre_recrutement, infos_perimetre_recrutement, prix_horaire_ttc, prix_total_TTC, duree_indicative, nombre_heures_centre, nombre_heures_entreprise, nombre_heures_total, detail_conditions_prise_en_charge, conventionnement, duree_conventionnee, organisme_formateur, organisme_financeur, financement_formation, nb_places, deroulement, moyens_pedagogiques, responsable_enseignement, nombre_heures_cm,  nombre_heures_td, nombre_heures_tp_tuteure, nombre_heures_tp_non_tuteure, nombre_heures_personnel, nombre_heures_hebdomadaire, id_formation])
                    .then((result) => {
                        res.json({result});
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    })
            })
            .catch(err => {
                console.log(err);
            })
    },
    modifyAction (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const rythme_formation = req.body.rythme_formation;
        const code_public_vise = req.body.code_public_vise;
        const info_public_vise = req.body.info_public_vise;
        const niveau_entree_obligatoire = req.body.niveau_entree_obligatoire;
        const modalites_alternance = req.body.modalites_alternance;
        const modalites_enseignement = req.body.modalites_enseignement;
        const conditions_specifiques = req.body.conditions_specifiques;
        const prise_en_charge_frais_possible = req.body.prise_en_charge_frais_possible;
        const lieu_de_formation = req.body.lieu_de_formation;
        const modalites_entrees_sorties = req.body.modalites_entrees_sorties;
        const url_action = req.body.url_action;
        const session = req.body.session;
        const adresse_information = req.body.adresse_information;
        const date_information = req.body.date_information;
        const restauration = req.body.restauration;
        const hebergement = req.body.hebergement;
        const transport = req.body.transport;
        const acces_handicapes = req.body.acces_handicapes;
        const langue_formation = req.body.langue_formation;
        const modalites_recrutement = req.body.modalites_recrutement;
        const modalites_pedagogiques = req.body.modalites_pedagogiques;
        const code_modalite_pedagogique = req.body.code_modalite_pedagogique;
        const frais_restants = req.body.frais_restants;
        const code_perimetre_recrutement = req.body.code_perimetre_recrutement;
        const infos_perimetre_recrutement = req.body.infos_perimetre_recrutement;
        const prix_horaire_ttc = req.body.prix_horaire_ttc;
        const prix_total_TTC = req.body.prix_total_TTC;
        const duree_indicative = req.body.duree_indicative;
        const nombre_heures_centre = req.body.nombre_heures_centre;
        const nombre_heures_entreprise = req.body.nombre_heures_entreprise;
        const nombre_heures_total = req.body.nombre_heures_total;
        const detail_conditions_prise_en_charge = req.body.detail_conditions_prise_en_charge;
        const conventionnement = req.body.conventionnement;
        const duree_conventionnee = req.body.duree_conventionnee;
        const organisme_formateur = req.body.organisme_formateur;
        const organisme_financeur = req.body.organisme_financeur;
        const financement_formation = req.body.financement_formation;
        const nb_places = req.body.nb_places;
        const deroulement = req.body.deroulement;
        const moyens_pedagogiques = req.body.moyens_pedagogiques;
        const responsable_enseignement = req.body.responsable_enseignement;
        const nombre_heures_cm = req.body.nombre_heures_cm;
        const nombre_heures_td = req.body.nombre_heures_td;
        const nombre_heures_tp_tuteure = req.body.nombre_heures_tp_tuteure;
        const nombre_heures_tp_non_tuteure = req.body.nombre_heures_tp_non_tuteure;
        const nombre_heures_personnel = req.body.nombre_heures_personnel;
        const nombre_heures_hebdomadaire = req.body.nombre_heures_hebdomadaire;
        const id_formation = req.body.id_formation;
        const action_id = req.params.id;
        let sql = 'UPDATE ACTION SET ';
        if(rythme_formation) {
            sql += "rythme_formation ='" + rythme_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(code_public_vise) {
            sql += "code_public_vise ='" + code_public_vise + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(info_public_vise) {
            sql += "info_public_vise ='" + info_public_vise + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(niveau_entree_obligatoire) {
            sql += "niveau_entree_obligatoire ='" + niveau_entree_obligatoire + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(modalites_alternance) {
            sql += "modalites_alternance ='" + modalites_alternance + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(modalites_enseignement) {
            sql += "modalites_enseignement ='" + modalites_enseignement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(conditions_specifiques) {
            sql += "conditions_specifiques ='" + conditions_specifiques + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(prise_en_charge_frais_possible) {
            sql += "prise_en_charge_frais_possible ='" + prise_en_charge_frais_possible + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(lieu_de_formation) {
            sql += "lieu_de_formation ='" + lieu_de_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(modalites_entrees_sorties) {
            sql += "modalites_entrees_sorties ='" + modalites_entrees_sorties + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(url_action) {
            sql += "url_action ='" + url_action + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(session) {
            sql += "session ='" + session + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(adresse_information) {
            sql += "adresse_information ='" + adresse_information + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(date_information) {
            sql += "date_information ='" + date_information + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(restauration) {
            sql += "restauration ='" + restauration + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(hebergement) {
            sql += "hebergement ='" + hebergement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(transport) {
            sql += "transport ='" + transport + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(acces_handicapes) {
            sql += "acces_handicapes ='" + acces_handicapes + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(langue_formation) {
            sql += "langue_formation ='" + langue_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(modalites_recrutement) {
            sql += "modalites_recrutement ='" + modalites_recrutement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(modalites_pedagogiques) {
            sql += "modalites_pedagogiques ='" + modalites_pedagogiques + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(code_modalite_pedagogique) {
            sql += "code_modalite_pedagogique ='" + code_modalite_pedagogique + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(frais_restants) {
            sql += "frais_restants ='" + frais_restants + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(code_perimetre_recrutement) {
            sql += "code_perimetre_recrutement ='" + code_perimetre_recrutement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(infos_perimetre_recrutement) {
            sql += "infos_perimetre_recrutement ='" + infos_perimetre_recrutement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(prix_horaire_ttc) {
            sql += "prix_horaire_ttc ='" + prix_horaire_ttc + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(prix_total_TTC) {
            sql += "prix_total_TTC ='" + prix_total_TTC + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(duree_indicative) {
            sql += "duree_indicative ='" + duree_indicative + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_centre) {
            sql += "nombre_heures_centre ='" + nombre_heures_centre + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_entreprise) {
            sql += "nombre_heures_entreprise ='" + nombre_heures_entreprise + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_total) {
            sql += "nombre_heures_total ='" + nombre_heures_total + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(detail_conditions_prise_en_charge) {
            sql += "detail_conditions_prise_en_charge ='" + detail_conditions_prise_en_charge + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(conventionnement) {
            sql += "conventionnement ='" + conventionnement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(duree_conventionnee) {
            sql += "duree_conventionnee ='" + duree_conventionnee + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(organisme_formateur) {
            sql += "organisme_formateur ='" + organisme_formateur + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(organisme_financeur) {
            sql += "organisme_financeur ='" + organisme_financeur + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(financement_formation) {
            sql += "financement_formation ='" + financement_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nb_places) {
            sql += "nb_places ='" + nb_places + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(deroulement) {
            sql += "deroulement ='" + deroulement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(moyens_pedagogiques) {
            sql += "moyens_pedagogiques ='" + moyens_pedagogiques + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(responsable_enseignement) {
            sql += "responsable_enseignement ='" + responsable_enseignement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_cm) {
            sql += "nombre_heures_cm ='" + nombre_heures_cm + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_td) {
            sql += "nombre_heures_td ='" + nombre_heures_td + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_tp_tuteure) {
            sql += "nombre_heures_tp_tuteure ='" + nombre_heures_tp_tuteure + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_tp_non_tuteure) {
            sql += "nombre_heures_tp_non_tuteure ='" + nombre_heures_tp_non_tuteure + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_personnel) {
            sql += "nombre_heures_personnel ='" + nombre_heures_personnel + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nombre_heures_hebdomadaire) {
            sql += "nombre_heures_hebdomadaire ='" + nombre_heures_hebdomadaire + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_formation) {
            sql += "id_formation ='" + id_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_action = " + action_id + ";";

        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    deleteAction (req, res) {
        const action_id = req.params.id;
        const sql = "DELETE FROM ACTION WHERE id_action = " + action_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    }
}

module.exports = Action;