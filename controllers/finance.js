const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);

const Finance = {
    getAllFinances (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM FINANCE")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    getFinance (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM FINANCE WHERE id_organisme_financeur = ?",[req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    insertFinance (req, res) {
        const id_organisme_financeur = req.body.id_organisme_financeur;
        const id_formation = req.body.id_formation;
        //res.json(req.body);
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO FINANCE (id_organisme_financeur, id_formation) VALUES (?, ?)",
                    [id_organisme_financeur, id_formation])
                    .then((result) => {
                        res.json({result});
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    })
            })
            .catch(err => {
                console.log(err);
            })
    },
    modifyFinance (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const id_organisme_financeur = req.body.id_organisme_financeur;
        const id_formation = req.body.id_formation;
        let sql = 'UPDATE FINANCE SET ';
        if(id_organisme_financeur) {
            sql += "id_organisme_financeur ='" + id_organisme_financeur  + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_formation) {
            sql += "id_formation ='" + id_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_organisme_financeur = " + id_organisme_financeur + ";";

        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    deleteFinance (req, res) {
        const finance_id = req.params.id;
        const sql = "DELETE FROM FINANCE WHERE id_organisme_financeur = " + finance_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    }
}

module.exports = Finance;