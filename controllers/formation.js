const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);

const Formation = {
    getAllFormations (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM FORMATION")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    getFormation (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM FORMATION WHERE id_formation = ?",[req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    insertFormation (req, res) {
        const intitule_formation = req.body.intitule_formation;
        const domaine_formation = req.body.domaine_formation;
        const objectif_formation = req.body.objectif_formation;
        const resultats_attendus = req.body.resultats_attendus;
        const contenu_formation = req.body.contenu_formation;
        const certifiante = req.body.certifiante;
        const contact_formation = req.body.contact_formation;
        const parcours_de_formation = req.body.parcours_de_formation;
        const code_niveau_entree = req.body.code_niveau_entree;
        const objectif_general_formation = req.body.objectif_general_formation;
        const certification = req.body.certification;
        const code_niveau_sortie = req.body.code_niveau_sortie;
        const url_formation = req.body.url_formation;
        const action = req.body.action;
        const liens_entreprise = req.body.liens_entreprise;
        const organisme_formation_responsable = req.body.organisme_formation_responsable;
        const credits_ects = req.body.credits_ects;
        const identifiant_module = req.body.identifiant_module;
        const positionnement = req.body.positionnement;
        const sous_modules = req.body.sous_modules;
        const modules_prerequis = req.body.modules_prerequis;
        const eligibilite_cpf = req.body.eligibilite_cpf;
        const validations = req.body.validations;
        const id_offres = req.body.id_offres;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO FORMATION (intitule_formation, domaine_formation, objectif_formation, resultats_attendus, contenu_formation, certifiante, contact_formation, parcours_de_formation, code_niveau_entree, objectif_general_formation, certification, code_niveau_sortie, url_formation, action, liens_entreprise, organisme_formation_responsable, credits_ects, identifiant_module, positionnement, sous_modules, modules_prerequis, eligibilite_cpf, validations, id_offres) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    [intitule_formation, domaine_formation, objectif_formation, resultats_attendus, contenu_formation, certifiante, contact_formation, parcours_de_formation, code_niveau_entree, objectif_general_formation, certification, code_niveau_sortie, url_formation, action, liens_entreprise, organisme_formation_responsable, credits_ects, identifiant_module, positionnement, sous_modules, modules_prerequis, eligibilite_cpf, validations, id_offres])
                    .then((result) => {
                        res.json({result});
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    })
            })
            .catch(err => {
                console.log(err);
            })
    },
    modifyFormation (req,res) {
        let count = 1;
        const limit = Object.keys(req.body).length;
        const intitule_formation = req.body.intitule_formation;
        const domaine_formation = req.body.domaine_formation;
        const objectif_formation = req.body.objectif_formation;
        const resultats_attendus = req.body.resultats_attendus;
        const contenu_formation = req.body.contenu_formation;
        const certifiante = req.body.certifiante;
        const contact_formation = req.body.contact_formation;
        const parcours_de_formation = req.body.parcours_de_formation;
        const code_niveau_entree = req.body.code_niveau_entree;
        const objectif_general_formation = req.body.objectif_general_formation;
        const certification = req.body.certification;
        const code_niveau_sortie = req.body.code_niveau_sortie;
        const url_formation = req.body.url_formation;
        const action = req.body.action;
        const liens_entreprise = req.body.liens_entreprise;
        const organisme_formation_responsable = req.body.organisme_formation_responsable;
        const credits_ects = req.body.credits_ects;
        const identifiant_module = req.body.identifiant_module;
        const positionnement = req.body.positionnement;
        const sous_modules = req.body.sous_modules;
        const modules_prerequis = req.body.modules_prerequis;
        const eligibilite_cpf = req.body.eligibilite_cpf;
        const validations = req.body.validations;
        const id_offres = req.body.id_offres;
        const formation_id = req.params.id;
        let sql = 'UPDATE FORMATION SET ';
        if(intitule_formation) {
            sql += "intitule_formation ='" + intitule_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(domaine_formation) {
            sql += "domaine_formation ='" + domaine_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(objectif_formation) {
            sql += "objectif_formation ='" + objectif_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(resultats_attendus) {
            sql += "resultats_attendus ='" + resultats_attendus + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(contenu_formation) {
            sql += "contenu_formation ='" + contenu_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(certifiante) {
            sql += "certifiante ='" + certifiante + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(contact_formation) {
            sql += "contact_formation ='" + contact_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(parcours_de_formation) {
            sql += "parcours_de_formation ='" + parcours_de_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(code_niveau_entree) {
            sql += "code_niveau_entree ='" + code_niveau_entree + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(objectif_general_formation) {
            sql += "objectif_general_formation ='" + objectif_general_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(certification) {
            sql += "certification ='" + certification + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(code_niveau_sortie) {
            sql += "code_niveau_sortie ='" + code_niveau_sortie + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(url_formation) {
            sql += "url_formation ='" + url_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(action) {
            sql += "action ='" + action + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(liens_entreprise) {
            sql += "liens_entreprise ='" + liens_entreprise + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(organisme_formation_responsable) {
            sql += "organisme_formation_responsable ='" + organisme_formation_responsable + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(credits_ects) {
            sql += "credits_ects ='" + credits_ects + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(identifiant_module) {
            sql += "identifiant_module ='" + identifiant_module + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(positionnement) {
            sql += "positionnement ='" + positionnement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(sous_modules) {
            sql += "sous_modules ='" + sous_modules + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(modules_prerequis) {
            sql += "modules_prerequis ='" + modules_prerequis + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(eligibilite_cpf) {
            sql += "eligibilite_cpf ='" + eligibilite_cpf + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(validations) {
            sql += "validations ='" + validations + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_offres) {
            sql += "id_offres ='" + id_offres + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_formation = " + formation_id + ";";

        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    deleteFormation (req, res) {
        const formation_id = req.params.id;
        const sql = "DELETE FROM FORMATION WHERE id_formation = " + formation_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    }
}

module.exports = Formation;