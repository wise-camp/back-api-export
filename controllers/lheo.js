const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);


const Lheo = {
    getAllLheo(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM LHEO")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    getLheo(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM LHEO WHERE id_lheo = ?", [req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    insertLheo (req, res) {
        const offres = req.body.offres;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO LHEO (offres) VALUES (?)",
                    [offres])
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    });
            })
            .catch(err => {
                console.log(err);
            });
    },
    modifyLheo (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const offres = req.body.offres;
        const lheo_id = req.params.id;
        let sql = 'UPDATE LHEO SET ';
        if(offres) {
            sql += "offres ='" + offres + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_lheo = " + lheo_id + ";";
        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    deleteLheo(req, res) {
        const lheo_id = req.params.id;
        const sql = "DELETE FROM LHEO WHERE id_lheo = " + lheo_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    }
};

module.exports = Lheo;
