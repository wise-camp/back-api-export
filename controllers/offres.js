const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);


const Offre = {
    getAllOffre(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM OFFRES")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    getOffre(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM OFFRES WHERE id_offres = ?", [req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    insertOffre (req, res) {
        const formation = req.body.formation;
        const id_lheo = req.body.id_lheo;
        const id_formation = req.body.formation;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO OFFRES (formation, id_lheo, id_formation) VALUES (?,?,?)",
                    [formation, id_lheo, id_formation])
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    });
            })
            .catch(err => {
                console.log(err);
            });
    },
    modifyOffre (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const formation = req.body.formation;
        const id_lheo = req.body.id_lheo;
        const id_formation = req.body.formation;
        const offre_id = req.params.id;
        let sql = 'UPDATE LHEO SET ';
        if(formation) {
            sql += "formation ='" + formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_lheo) {
            sql += "id_lheo ='" + id_lheo + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_formation) {
            sql += "id_formation ='" + id_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_offres = " + offre_id + ";";
        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    deleteOffre(req, res) {
        const offre_id = req.params.id;
        const sql = "DELETE FROM OFFRES WHERE id_offres = " + offre_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    }
};

module.exports = Offre;
