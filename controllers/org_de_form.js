const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);

const Org = {
    getAllOrg (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM ORGANISME_DE_FORMATION_RESPONSABLE")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    getOrg(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM ORGANISME_DE_FORMATION_RESPONSABLE WHERE id_organisme_de_formation_responsable = ?", [req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    insertOrg(req, res) {
        const numero_activite = req.body.numero_activite;
        const siret_organisme_formation = req.body.siret_organisme_formation;
        const nom_organisme = req.body.nom_organisme;
        const raison_sociale = req.body.raison_sociale;
        const coordonnees_organisme = req.body.coordonnees_organisme;
        const contact_organisme = req.body.contact_organisme;
        const renseignements_specifiques = req.body.renseignements_specifiques;
        const potentiel = req.body.potentiel;
        const agreement_datadock = req.body.agreement_datadock;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO `ORGANISME_DE_FORMATION_RESPONSABLE`( `numero_activite`, `siret_organisme_formation`, `nom_organisme`, `raison_sociale`, `coordonnees_organisme`, `contact_organisme`, `renseignements_specifiques`, `potentiel`, `agreement_datadock`) VALUES (?,?,?,?,?,?,?,?,?);",
                    [numero_activite, siret_organisme_formation, nom_organisme, raison_sociale, coordonnees_organisme, contact_organisme, renseignements_specifiques, potentiel, agreement_datadock])
                    .then((result) => {
                        res.json({result});
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    });
            })
            .catch(err => {
                console.log(err);
            });
    },
    modifyOrg (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const numero_activite = req.body.numero_activite;
        const siret_organisme_formation = req.body.siret_organisme_formation;
        const nom_organisme = req.body.nom_organisme;
        const raison_sociale = req.body.raison_sociale;
        const coordonnees_organisme = req.body.coordonnees_organisme;
        const contact_organisme = req.body.contact_organisme;
        const renseignements_specifiques = req.body.renseignements_specifiques;
        const potentiel = req.body.potentiel;
        const agreement_datadock = req.body.agreement_datadock;
        const org_id = req.params.id;
        let sql = 'UPDATE ORGANISME_DE_FORMATION_RESPONSABLE SET ';
        if(numero_activite) {
            sql += "numero_activite ='" + numero_activite + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(siret_organisme_formation) {
            sql += "siret_organisme_formation ='" + siret_organisme_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nom_organisme) {
            sql += "nom_organisme ='" + nom_organisme + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(raison_sociale != null) {
            sql += "raison_sociale ='" + raison_sociale + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(coordonnees_organisme != null) {
            sql += "coordonnees_organisme ='" + coordonnees_organisme + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(contact_organisme) {
            sql += "contact_organisme ='" + contact_organisme + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(renseignements_specifiques) {
            sql += "renseignements_specifiques ='" + renseignements_specifiques + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(potentiel) {
            sql += "potentiel ='" + potentiel + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(agreement_datadock) {
            sql += "agreement_datadock ='" + agreement_datadock + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_organisme_de_formation_responsable = " + org_id + ";";
        // res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    deleteOrg(req, res) {
        const org_id = req.params.id;
        const sql = "DELETE FROM ORGANISME_DE_FORMATION_RESPONSABLE WHERE id_organisme_de_formation = " + org_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
}

module.exports = Org;
