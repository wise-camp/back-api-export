const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);


const Org = {
    getAllOrg(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM ORGANISME_FINANCEUR")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    getOrg(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM ORGANISME_FINANCEUR WHERE id_organisme_financeur = ?", [req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    insertOrg (req, res) {
        const code_financeur = req.body.code_financeur;
        const nb_places_financees = req.body.nb_places_financees;
        const SIRET_formateur = req.body.SIRET_formateur;
        const raison_sociale_formateur = req.body.raison_sociale_formateur;
        const contact_formateur = req.body.contact_formateur;
        const potentiel = req.body.potentiel;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO ORGANISME_FINANCEUR (code_financeur, nb_places_financees, SIRET_formateur, raison_sociale_formateur, contact_formateur, potentiel) VALUES (?,?,?,?,?,?)",
                    [code_financeur, nb_places_financees, SIRET_formateur, raison_sociale_formateur, contact_formateur, potentiel])
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    });
            })
            .catch(err => {
                console.log(err);
            });
    },
    modifyOrg (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const code_financeur = req.body.code_financeur;
        const nb_places_financees = req.body.nb_places_financees;
        const SIRET_formateur = req.body.SIRET_formateur;
        const raison_sociale_formateur = req.body.raison_sociale_formateur;
        const contact_formateur = req.body.contact_formateur;
        const potentiel = req.body.potentiel;
        const org_id = req.params.id;
        let sql = 'UPDATE ORGANISME_FINANCEUR SET ';
        if(code_financeur) {
            sql += "code_financeur ='" + code_financeur + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(nb_places_financees) {
            sql += "nb_places_financees ='" + nb_places_financees + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(SIRET_formateur) {
            sql += "SIRET_formateur ='" + SIRET_formateur + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(raison_sociale_formateur) {
            sql += "raison_sociale_formateur ='" + raison_sociale_formateur + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(contact_formateur) {
            sql += "contact_formateur ='" + contact_formateur + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(potentiel) {
            sql += "potentiel ='" + potentiel + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_organisme_financeur = " + org_id + ";";
        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    deleteOrg(req, res) {
        const org_id = req.params.id;
        const sql = "DELETE FROM ORGANISME_FINANCEUR WHERE id_organisme_financeur = " + org_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    }
};

module.exports = Org;
