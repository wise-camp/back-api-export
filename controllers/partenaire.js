const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);


const Part = {
    getAllPart(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM PARTENAIRE")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    getPart(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM PARTENAIRE WHERE id_partenaire = ?", [req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    insertPart (req, res) {
        const nom_partenaire = req.body.nom_partenaire;
        const logo = req.body.logo;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO PARTENAIRE (nom_partenaire, logo) VALUES (?,?)",
                    [nom_partenaire, logo])
                    .then((result) => {
                        res.json({result});
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    });
            })
            .catch(err => {
                console.log(err);
            });
    },
    modifyPart (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const nom_partenaire = req.body.nom_partenaire;
        const logo = req.body.logo;
        const part_id = req.params.id;
        let sql = 'UPDATE PARTENAIRE SET ';
        if(nom_partenaire) {
            sql += "nom_partenaire ='" + nom_partenaire + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(logo) {
            sql += "logo ='" + logo + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_partenaire = " + part_id + ";";
        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    deletePart(req, res) {
        const part_id = req.params.id;
        const sql = "DELETE FROM PARTENAIRE WHERE id_partenaire = " + part_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    }
};

module.exports = Part;
