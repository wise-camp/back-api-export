const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);

const Responsable = {
    getAllResponsables (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM RESPONSABLE_DE")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    getResponsable (req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM RESPONSABLE_DE WHERE id_formation = ?",[req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    insertResponsable (req, res) {
        const id_formation = req.body.id_formation;
        const id_organisme_de_formation_responsable = req.body.id_organisme_de_formation_responsable;
        //res.json(req.body);
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO RESPONSABLE_DE (id_formation, id_organisme_de_formation_responsable) VALUES (?, ?)",
                    [id_formation, id_organisme_de_formation_responsable])
                    .then((result) => {
                        res.json({result});
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    })
            })
            .catch(err => {
                console.log(err);
            })
    },
    modifyResponsable (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const id_formation = req.body.id_formation;
        const id_organisme_de_formation_responsable = req.body.id_organisme_de_formation_responsable;
        let sql = 'UPDATE RESPONSABLE_DE SET ';
        if(id_formation) {
            sql += "id_formation ='" + id_formation  + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_organisme_de_formation_responsable) {
            sql += "id_organisme_de_formation_responsable ='" + id_organisme_de_formation_responsable + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_formation = " + id_formation + ";";

        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    },
    deleteResponsable (req, res) {
        const formation_id = req.params.id;
        const sql = "DELETE FROM RESPONSABLE_DE WHERE id_formation = " + formation_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            })
    }
}

module.exports = Responsable;