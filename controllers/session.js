const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);

const Session = {
    getAllSessions(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM SESSION")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    getSession(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM SESSION WHERE id_session = ?", [req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    insertSession (req, res) {
        const periode = req.body.periode;
        const adresse_inscription = req.body.adresse_inscription;
        const modalites_inscription = req.body.modalites_inscription;
        const periode_inscription = req.body.periode_inscription;
        const etat_recrutement = req.body.etat_recrutement;
        const id_formation = req.body.id_formation;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO SESSION (periode, adresse_inscription, modalites_inscription, periode_inscription, etat_recrutement, id_formation) VALUES (?, ?, ?, ?, ?, ?)",
                    [periode, adresse_inscription, modalites_inscription, periode_inscription, etat_recrutement, id_formation])
                    .then((result) => {
                        res.json({result});
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    });
            })
            .catch(err => {
                console.log(err);
            });
    },
    modifySession (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const periode = req.body.periode;
        const adresse_inscription = req.body.adresse_inscription;
        const modalites_inscription = req.body.modalites_inscription;
        const periode_inscription = req.body.periode_inscription;
        const etat_recrutement = req.body.etat_recrutement;
        const id_formation = req.body.id_formation;
        const session_id = req.params.id;
        let sql = 'UPDATE SESSION SET ';
        if(periode) {
            sql += "periode ='" + periode + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(adresse_inscription) {
            sql += "adresse_inscription ='" + adresse_inscription + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(modalites_inscription) {
            sql += "modalites_inscription ='" + modalites_inscription + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(periode_inscription != null) {
            sql += "periode_inscription ='" + periode_inscription + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(etat_recrutement != null) {
            sql += "etat_recrutement ='" + etat_recrutement + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_formation) {
            sql += "id_formation ='" + id_formation + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_session = " + session_id + ";";
        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    deleteSession(req, res) {
        const session_id = req.params.id;
        const sql = "DELETE FROM SESSION WHERE id_session = " + session_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    }
};

module.exports = Session;