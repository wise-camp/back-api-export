const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);


const Token = {
    getAllToken(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM TOKEN")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    getToken(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM TOKEN WHERE id_token = ?", [req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    insertToken (req, res) {
        const cle = req.body.cle;
        const date_de_depart = req.body.date_de_depart;
        const id_utilisateur = req.body.id_utilisateur;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO TOKEN (cle, date_de_depart, id_utilisateur) VALUES (?,?,?)",
                    [cle, date_de_depart, id_utilisateur])
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    });
            })
            .catch(err => {
                console.log(err);
            });
    },
    modifyToken (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const cle = req.body.cle;
        const date_de_depart = req.body.date_de_depart;
        const id_utilisateur = req.body.id_utilisateur;
        const token_id = req.params.id;
        let sql = 'UPDATE TOKEN SET ';
        if(cle) {
            sql += "cle ='" + cle + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(date_de_depart) {
            sql += "date_de_depart ='" + date_de_depart + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_utilisateur) {
            sql += "id_utilisateur ='" + id_utilisateur + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_token = " + token_id + ";";
        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    deleteToken(req, res) {
        const token_id = req.params.id;
        const sql = "DELETE FROM TOKEN WHERE id_token = " + token_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    }
};

module.exports = Token;
