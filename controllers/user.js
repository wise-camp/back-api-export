const mariadb = require('mariadb');
const databaseOptions = require('../config/config');
const pool = mariadb.createPool(databaseOptions);
// Load input validation
const validateRegisterInput = require("../validation/register");
const validateLoginInput = require("../validation/login");
const uuidv4 = require('uuid/v4');

const User = {
    getAllUsers(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM UTILISATEUR")
                    .then((rows) => {
                        // console.log(rows); //[ {val: 1}, meta: ... ]
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    getUser(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM UTILISATEUR WHERE id_utilisateur = ?", [req.params.id])
                    .then((rows) => {
                        res.json(rows);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    insertUser (req, res) {
        const nom = req.body.nom;
        const mail = req.body.mail;
        const mot_de_passe = req.body.mot_de_passe;
        const administrateur = req.body.administrateur;
        const id_token = req.body.id_token || null;
        const id_partenaire = req.body.id_partenaire || null;
        pool.getConnection()
            .then(conn => {
                conn.query("INSERT INTO UTILISATEUR (mail, nom, mot_de_passe, administrateur, id_token, id_partenaire) VALUES (?, ?, ?, ?, ?, ?)",
                    [mail, nom, mot_de_passe, administrateur, id_token, id_partenaire])
                    .then((result) => {
                        res.json({result});
                        conn.end();
                    })
                    .catch(err => {
                        console.log(err);
                        conn.end();
                    });
            })
            .catch(err => {
                console.log(err);
            });
    },
    modifyUser (req,res) {
        let count = 0;
        const limit = Object.keys(req.body).length;
        const nom = req.body.nom;
        const mail = req.body.mail;
        const mot_de_passe = req.body.mot_de_passe;
        const administrateur = req.body.administrateur;
        const id_token = req.body.id_token;
        const id_partenaire = req.body.id_partenaire;
        const user_id = req.params.id;
        let sql = 'UPDATE UTILISATEUR SET ';
        if(nom) {
            sql += "nom ='" + nom + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(mail) {
            sql += "mail ='" + mail + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(mot_de_passe) {
            sql += "mot_de_passe ='" + mot_de_passe + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(administrateur != null) {
            sql += "administrateur ='" + administrateur + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_token != null) {
            sql += "id_token ='" + id_token + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        if(id_partenaire) {
            sql += "id_partenaire ='" + id_partenaire + "'";
            count++;
            if (count < limit) sql += ", ";
        }
        sql += " WHERE id_utilisateur = " + user_id + ";";
        //res.json({sql: sql, count: count, limit: limit});
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    deleteUser(req, res) {
        const user_id = req.params.id;
        const sql = "DELETE FROM UTILISATEUR WHERE id_utilisateur = " + user_id + ";"
        // res.json(sql);
        pool.getConnection()
            .then(conn => {
                conn.query(sql)
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(404).json(err);
                        conn.end();
                    })
            })
            .catch(err => {
                res.status(404).json(err);
            });
    },
    loginAccount(req, res) {
        // Form validation
        const { errors, isValid } = validateLoginInput(req.body);
        // Check validation
        if (!isValid) {
            return res.status(400).json(errors);
        }
        else {
            pool.getConnection()
                .then(conn => {
                    // Check if user exists
                    conn.query("SELECT * FROM UTILISATEUR WHERE mail = '" + req.body.email + "'")
                        .then((users) => {
                            if (users.length > 0 && users[0].mot_de_passe === req.body.password) {

                                // Cr�ation token maison
                                var guidKey = uuidv4();
                                var insertToken = "REPLACE INTO TOKEN (cle, date_de_depart, id_utilisateur) VALUES ('" + guidKey + "', " + " NOW() ," + users[0].id_utilisateur + ")";
                                conn.query(insertToken)
                                    .then((result) => {
                                        if (result.insertId)
                                            conn.query("SELECT * FROM TOKEN WHERE id_token = " + result.insertId)
                                                .then((token) => {
                                                    if (token[0]) {
                                                        var connectedUser = users[0];
                                                        var myToken = token[0];
                                                        myToken.date_de_depart = myToken.date_de_depart.toLocaleString('fr-FR');
                                                        connectedUser.token = myToken;
                                                        res.status(200).json({ user: connectedUser });
                                                        conn.end();
                                                    }
                                                    else {
                                                        res.status(405).json({ result: "Error get token" });
                                                        conn.end();
                                                    }

                                                }).catch(err => {
                                                    res.status(405).json({
                                                        result: "Error get token",
                                                        error: err
                                                    });
                                                    conn.end();
                                                });
                                        else {
                                            res.status(405).json({ result: "Error get token" });
                                            conn.end();
                                        }
                                    })
                                    .catch(err => {
                                        res.status(403).json(
                                            {
                                                error: err,
                                                result: "Error inserting token"
                                            });
                                        conn.end();
                                    });
                            }
                            else {
                                if (users.length > 0)
                                    res.status(401).json({ result: "Incorrect password" });
                                else 
                                    res.status(402).json({ result: "Incorrect email" });
                                conn.end();
                            }
                        })
                        .catch(err => {
                            res.status(404).json(
                                {
                                    error: err,
                                    result: "User does not exist"
                                });
                            conn.end();
                        });
                })
                .catch(err => {
                    res.status(500).json({ error: err });
                });
        }
    },
    logoutAccount(req, res) {
        pool.getConnection()
            .then(conn => {
                conn.query("DELETE FROM TOKEN WHERE cle = ?", [req.body.cle])
                    .then((result) => {
                        res.json(result);
                        conn.end();
                    })
                    .catch(err => {
                        res.status(500).json({ error: err });
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(500).json({ error: err })
                conn.end();
            });
    },
    checkConnected (req, res) {
        const key = req.params.key;
        pool.getConnection()
            .then(conn => {
                conn.query("SELECT * FROM TOKEN WHERE cle = ?", [key])
                    .then( (rows) => {
                        const token = rows[0];


                        let dateTimeParts= token.date_de_depart.split(/[- :]/); // regular expression split that creates array with: year, month, day, hour, minutes, seconds values
                        dateTimeParts[1]--; // monthIndex begins with 0 for January and ends with 11 for December so we need to decrement by one
                        dateTimeParts[3]++;
                        const dateObject = new Date(...dateTimeParts); // our Date object
                        const dateNow = new Date()
                        if(dateObject < dateNow) {
                            conn.query("SELECT * FROM UTILISATEUR WHERE id_utilisateur = ?", [token.id_utilisateur])
                                .then( (users) => {
                                    const user = users[0];
                                    res.json({
                                        message: 'The user is still connected',
                                        user: user
                                    });
                                    conn.end();
                                })
                                .catch(err => {
                                    res.status(500).json({ error: err });
                                    conn.end();
                                });
                        }else{
                            conn.query("DELETE FROM TOKEN WHERE cle = ?",[token.cle])
                                .then((result) => {
                                    res.json({
                                        result: result,
                                        message: "Le token de l'utilisateur n'est plus valide"
                                    });
                                    conn.end();
                                })
                                .catch(err => {
                                    res.status(404).json(err);
                                    conn.end();
                                })
                        }
                    })
                    .catch(err => {
                        res.status(500).json({ error: err });
                        conn.end();
                    });
            })
            .catch(err => {
                res.status(500).json({ error: err });
                conn.end();
            });
    }
};

module.exports = User;
