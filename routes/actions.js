const express = require("express");
const router = express.Router();

const action = require('../controllers/action');

//Actions
router.get('/', action.getAllActions);
router.get('/:id([0-9]+)', action.getAction);
router.post('/', action.insertAction);
router.put('/:id([0-9]+)', action.modifyAction);
router.delete('/:id([0-9]+)', action.deleteAction);


module.exports = router;