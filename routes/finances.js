const express = require("express");
const router = express.Router();

const action = require('../controllers/finance');

//Finances
router.get('/', action.getAllFinances);
router.get('/:id([0-9]+)', action.getFinance);
router.post('/', action.insertFinance);
router.put('/:id([0-9]+)', action.modifyFinance);
router.delete('/:id([0-9]+)', action.deleteFinance);


module.exports = router;