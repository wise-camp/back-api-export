const express = require("express");
const router = express.Router();

const formation = require('../controllers/formation');

//Formations
router.get('/', formation.getAllFormations );
router.get('/:id([0-9]+)', formation.getFormation );
router.post('/', formation.insertFormation );
router.put('/:id([0-9]+)', formation.modifyFormation );
router.delete('/:id([0-9]+)', formation.deleteFormation );


module.exports = router;