const express = require("express");
const router = express.Router();
const lheo = require('../controllers/lheo');

router.get('/', lheo.getAllLheo );
router.get('/:id([0-9]+)', lheo.getLheo );
router.post('/', lheo.insertLheo );
router.put('/:id([0-9]+)', lheo.modifyLheo );
router.delete('/:id([0-9]+)', lheo.deleteLheo );

module.exports = router;
