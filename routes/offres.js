const express = require("express");
const router = express.Router();
const offre = require('../controllers/offres');

router.get('/', offre.getAllOffre);
router.get('/:id([0-9]+)', offre.getOffre );
router.post('/', offre.insertOffre );
router.put('/:id([0-9]+)', offre.modifyOffre );
router.delete('/:id([0-9]+)', offre.deleteOffre );

module.exports = router;
