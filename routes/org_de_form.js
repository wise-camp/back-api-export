const express = require("express");
const router = express.Router();
const org = require('../controllers/org_de_form');

router.get('/', org.getAllOrg );
router.get('/:id([0-9]+)', org.getOrg );
router.post('/', org.insertOrg );
router.put('/:id([0-9]+)', org.modifyOrg );
router.delete('/:id([0-9]+)', org.deleteOrg );

module.exports = router;
