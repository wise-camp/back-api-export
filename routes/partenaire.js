const express = require("express");
const router = express.Router();
const part = require('../controllers/partenaire');

router.get('/', part.getAllPart );
router.get('/:id([0-9]+)', part.getPart );
router.post('/', part.insertPart );
router.put('/:id([0-9]+)', part.modifyPart );
router.delete('/:id([0-9]+)', part.deletePart );

module.exports = router;
