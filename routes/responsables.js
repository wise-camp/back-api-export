const express = require("express");
const router = express.Router();

const action = require('../controllers/responsable');

//Responsables
router.get('/', action.getAllResponsables);
router.get('/:id([0-9]+)', action.getResponsable);
router.post('/', action.insertResponsable);
router.put('/:id([0-9]+)', action.modifyResponsable);
router.delete('/:id([0-9]+)', action.deleteResponsable);


module.exports = router;