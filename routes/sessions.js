const express = require("express");
const router = express.Router();

const action = require('../controllers/session');

//Sessions
router.get('/', action.getAllSessions);
router.get('/:id([0-9]+)', action.getSession);
router.post('/', action.insertSession);
router.put('/:id([0-9]+)', action.modifySession);
router.delete('/:id([0-9]+)', action.deleteSession);


module.exports = router;