const express = require("express");
const router = express.Router();
const token = require('../controllers/token');

router.get('/', token.getAllToken );
router.get('/:id([0-9]+)', token.getToken );
router.post('/', token.insertToken );
router.put('/:id([0-9]+)', token.modifyToken );
router.delete('/:id([0-9]+)', token.deleteToken );

module.exports = router;
