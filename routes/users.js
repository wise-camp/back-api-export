const express = require("express");
const router = express.Router();
const md5 = require("md5");
const jwt = require("jsonwebtoken");

// Load Key Token
const keys = require('../config/keys');
const user = require('../controllers/user');

//Users
router.get('/', user.getAllUsers );
router.get('/:id([0-9]+)', user.getUser );
router.post('/', user.insertUser );
router.put('/:id([0-9]+)', user.modifyUser );
router.delete('/:id([0-9]+)', user.deleteUser );

router.post('/login', user.loginAccount );
router.post('/logout', user.logoutAccount );
router.get('/state/:key', user.checkConnected );




module.exports = router;
